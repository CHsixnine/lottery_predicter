import numpy as np
import pandas as pd
import os
from datetime import datetime, timedelta
from tqdm import tqdm

def time_parser(year="",month_day="" ,year_month_day="" , years="new"):
    if years == "new":
        date_time = year_month_day.replace("/", "-")
        return datetime.strptime(date_time, '%Y-%m-%d')
    elif  years == "old":
        date_time = str(int(year)) + "-" + month_day.replace("月", "-").replace("日", "")
        return datetime.strptime(date_time, '%Y-%m-%d')

def data_parser(all_data, data, years="new"):
    if years == 'new':
        this_data = {"Time": [], "ball_1": [], "ball_2": [], "ball_3": [], "ball_4": [], "ball_5": [], "ball_6": [], "2_ball": []}
        for i in range(len(data)):
            try:
                this_data["Time"].append(time_parser(year_month_day=data["開獎日期"][i] , years="new"))
                this_data["ball_1"].append(int(data["獎號1"][i]))
                this_data["ball_2"].append(int(data["獎號2"][i]))
                this_data["ball_3"].append(int(data["獎號3"][i]))
                this_data["ball_4"].append(int(data["獎號4"][i]))
                this_data["ball_5"].append(int(data["獎號5"][i]))
                this_data["ball_6"].append(int(data["獎號6"][i]))
                this_data["2_ball"].append(int(data["第二區"][i]))
            except Exception as e:
                print(e)
        this_data = pd.DataFrame(this_data)
        return pd.concat([all_data, this_data])
    elif years == 'old':
        this_data = {"Time": [], "ball_1": [], "ball_2": [], "ball_3": [], "ball_4": [], "ball_5": [], "ball_6": [], "2_ball": []}
        for i in range(len(data)):
            try:
                this_data["Time"].append(time_parser(year=data["年份"][i], month_day=data["日期"][i], years="old"))
                this_data["ball_1"].append(int(data["球號1"][i]))
                this_data["ball_2"].append(int(data["球號2"][i]))
                this_data["ball_3"].append(int(data["球號3"][i]))
                this_data["ball_4"].append(int(data["球號4"][i]))
                this_data["ball_5"].append(int(data["球號5"][i]))
                this_data["ball_6"].append(int(data["球號6"][i]))
                this_data["2_ball"].append(int(data["特號"][i]))
            except Exception as e:
                print(e)
        this_data = pd.DataFrame(this_data)
        return pd.concat([all_data, this_data])

if __name__ == "__main__":
    data_list = sorted(os.listdir("./data"))
    all_data = pd.DataFrame({"Time": [], "ball_1": [], "ball_2": [], "ball_3": [], "ball_4": [], "ball_5": [], "ball_6": [], "2_ball": []})
    
    for i in tqdm(data_list):
        print(i)
        data = pd.read_csv("./data/"+i)
        if int(i.split('_')[1].split('.')[0]) >= 2014:

            all_data = data_parser(all_data, data, "new")
            pass
        else:
            all_data = data_parser(all_data, data, "old")
    all_data = all_data.astype(dtype={"ball_1": "int", "ball_2": "int", "ball_3": "int", "ball_4": "int", "ball_5": "int", "ball_6": "int", "2_ball": "int", })
    all_data.to_csv('./all_data.csv', index=False)