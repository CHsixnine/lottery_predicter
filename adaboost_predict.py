import numpy as np
import pandas as pd
import os
from datetime import datetime, timedelta
from tqdm import tqdm
from sklearn import ensemble

def buildTrain(data):
    x = []
    y = []
    for i in range(data.shape[0]-51):
        x.append(data.iloc[i:i+50])
        y.append(data.iloc[i+50])
    return x, y

if __name__ == "__main__":
    data = pd.read_csv('./all_data_class.csv')
    # data = data.drop(["Time"], axis=1).copy()
    data = data.set_index('Time') 
    test = data[int(data.shape[0]*0.95):].copy()
    train = data[:int(data.shape[0]*0.95)].copy()
    x_train, y_train = buildTrain(train)
    x_test, y_test = buildTrain(test)

    x_train = x_train[0]
    y_train = y_train[0]

    x_test = x_test[0]
    y_test = y_test[0]
    
    boost = ensemble.AdaBoostClassifier(n_estimators = 100)
    boost_fit = boost.fit(x_train, y_train)

    y_hat = boost.predict(s_test)

    accuracy = metrics.accuracy_score(y_test, y_hat)
    print(accuracy)





