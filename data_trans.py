import numpy as np
import pandas as pd
import os
from datetime import datetime, timedelta
from tqdm import tqdm

if __name__ == "__main__":
    data = pd.read_csv('./所有年.csv')
    table = {"Time":[]}
    for i in range(1,39):
        table['1_ball_'+str(i)] = []
    for i in range(1,9):
        table['2_ball_'+str(i)] = []
    for i in range(data.shape[0]):
        table["Time"].append(data["Time"][i])
        for j in list(table.keys())[1:39]:
            if data["ball_1"][i] == int(j.split('_')[2]):
                table[j].append(1)
            elif data["ball_2"][i] == int(j.split('_')[2]):
                table[j].append(1)
            elif data["ball_3"][i] == int(j.split('_')[2]):
                table[j].append(1)
            elif data["ball_4"][i] == int(j.split('_')[2]):
                table[j].append(1)
            elif data["ball_5"][i] == int(j.split('_')[2]):
                table[j].append(1)
            elif data["ball_6"][i] == int(j.split('_')[2]):
                table[j].append(1)
            else:
                table[j].append(0)
        for j in list(table.keys())[39:]:
            if data["2_ball"][i] == int(j.split('_')[2]):
                table[j].append(1)
            else:
                table[j].append(0)
    table_type = {}
    for i in list(table.keys())[1:]:
        table_type[i] = "int"

    print(table_type)
    all_data = pd.DataFrame(table)
    all_data = all_data.astype(dtype=table_type)
    all_data.to_csv('./all_data_class.csv', index=False)