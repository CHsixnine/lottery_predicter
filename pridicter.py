from typing import Mapping
import numpy as np
import pandas as pd
import os
from datetime import datetime, timedelta

class TableStructure:
    balls = {
        'ball1': ['球號 1', '獎號1'],
        'ball2': ['球號 2', '獎號2'],
        'ball3': ['球號 3', '獎號3'],
        'ball4': ['球號 4', '獎號4'],
        'ball5': ['球號 5', '獎號5'],
        'ball6': ['球號 6', '獎號6'],
        'balls': ['特號', '特號']
    }
    
    date_time = ['開獎日期', '年份', '日期']

def table_parser(table, data, year):
    for i in range(data.shape[1]):
        new_table = {"Time":[]}
        for j in range(1,39):
                new_table[str(j)] = []
    if year == 'new':
        for i in range(len(data)):
            new_table["Time"].append('')
            print(new_table.keys())
            for j in list(new_table.keys())[1:]:
                if int(j) == int(data['獎號1'][i]):
                    new_table[j].append(1)
                elif int(j) == int(data['獎號2'][i]):
                    new_table[j].append(1)
                elif int(j) == int(data['獎號3'][i]):
                    new_table[j].append(1)
                elif int(j) == int(data['獎號4'][i]):
                    new_table[j].append(1)
                elif int(j) == int(data['獎號5'][i]):
                    new_table[j].append(1)
                elif int(j) == int(data['獎號6'][i]):
                    new_table[j].append(1)
                else:
                    new_table[j].append(0)
        a = pd.DataFrame(new_table)
        return pd.concat([table, a])
    elif year == 'old':
        for i in range(len(data)):
            new_table["Time"].append("")
            for j in list(new_table.keys())[1:]:
                if int(j) == int(data['球號1'][i]):
                    new_table[j].append(1)
                elif int(j) == int(data['球號2'][i]):
                    new_table[j].append(1)
                elif int(j) == int(data['球號3'][i]):
                    new_table[j].append(1)
                elif int(j) == int(data['球號4'][i]):
                    new_table[j].append(1)
                elif int(j) == int(data['球號5'][i]):
                    new_table[j].append(1)
                elif int(j) == int(data['球號6'][i]):
                    new_table[j].append(1)
                else:
                    new_table[j].append(0)
        a = pd.DataFrame(new_table)
        return pd.concat([table, a])

if __name__ == "__main__":
    data_list = os.listdir("./data")
    print(data_list)
    table = {"Time":[]}
    for i in range(1,39):
        table[str(i)] = []
    table = pd.DataFrame(table)

    for i in data_list:
        data = pd.read_csv("./data/"+i)
        if int(i.split('_')[1].split('.')[0]) >= 2014:
            table = table_parser(table, data, "new")
            pass
        else:
            table = table_parser(table, data, "old")

    table.to_csv('./old_year.csv')
        