import numpy as np
import pandas as pd
import os
from datetime import datetime, timedelta
from tqdm import tqdm

if __name__ == "__main__":
    data = pd.read_csv('./all_data_class.csv')
    table = {"Time":[]}
    for i in range(1,39):
        table['1_ball_'+str(i)] = []
    for i in range(1,9):
        table['2_ball_'+str(i)] = []

    for j in list(table.keys()):
        table[j].append(data[j][0])

    for i in range(1, data.shape[0]):
        table["Time"].append(data["Time"][i])
        for j in list(table.keys())[1:]:
            table[j].append(table[j][i-1]+data[j][i])

        
    table_type = {}
    for i in list(table.keys())[1:]:
        table_type[i] = "int"

    print(table_type)
    all_data = pd.DataFrame(table)
    all_data = all_data.astype(dtype=table_type)
    all_data.to_csv('./all_data_class_sum.csv', index=False)